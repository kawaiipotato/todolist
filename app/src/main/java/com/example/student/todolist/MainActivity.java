package com.example.student.todolist;
        import android.app.Fragment;
        import android.app.FragmentTransaction;
        import android.os.Bundle;
        import java.util.ArrayList;

public class MainActivity extends SingleFragmentActivity implements ActivityCallback{
            public ArrayList<ToDoItem> toDoItems = new ArrayList();
            public int currentItem;

            @Override
            public void onPostSelected(int pos) {
                currentItem = pos;
                Fragment newFragment = new ItemFragment();
                FragmentTransaction transaction = getFragmentManager().beginTransaction();
                transaction.replace(R.id.fragment_container, newFragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }

    @Override
    protected Fragment createFragment() {
        return new ToDoFragment();
    }

            @Override
            protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_fragment);
            }



}

