package com.example.student.todolist;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ItemViewFragment extends Fragment {
    public MainActivity activity;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View layoutView =inflater.inflate(R.layout.fragment_todo_list, container, false);


        TextView tv1 = (TextView) layoutView.findViewById(R.id.t1);
        TextView tv2 = (TextView) layoutView.findViewById(R.id.t2);
        TextView tv3 = (TextView) layoutView.findViewById(R.id.t3);
        TextView tv4 = (TextView) layoutView.findViewById(R.id.t4);

        tv1.setText("Name: " + activity.toDoItems.get(activity.currentItem).title);
        tv2.setText("Name: " + activity.toDoItems.get(activity.currentItem).title);
        tv3.setText("Name: " + activity.toDoItems.get(activity.currentItem).title);
        tv4.setText("Name: " + activity.toDoItems.get(activity.currentItem).title);
    return layoutView;

    }
}
