package com.example.student.todolist;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class ToDoFragment extends Fragment {
    private RecyclerView recyclerView;
    private MainActivity activity;
    public ArrayList<ToDoItem> toDoItems = new ArrayList();
    public ActivityCallback activityCallBack;
//    private ActivityCallback activiyCallBack;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
//        activiyCallBack = (ActivityCallback)activity;
        this.activity = (MainActivity)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        activiyCallBack = null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.fragment_todo_list, container, false);

        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));//uses layout manager to show us how the adapter works and how it is displayed on the screem

        ToDoItem i1 = new ToDoItem("number", "one", "in", "list");
        ToDoItem i2 = new ToDoItem("nummber", "two", "in", "list");
        ToDoItem i3 = new ToDoItem("nummmber", "three", "in", "list");
        ToDoItem i4= new ToDoItem("nummmmber", "four", "in", "list");


        activity.toDoItems.add(i1);
        activity.toDoItems.add(i2);
        activity.toDoItems.add(i3);
        activity.toDoItems.add(i4);


//        new RedditPostTask().execute(this);//this connection does not happen instantly. Async tasks work like this
        TodoAdapter adapter = new TodoAdapter(activity.toDoItems, activityCallBack);
        recyclerView.setAdapter(adapter);

        return view;
    }
}
