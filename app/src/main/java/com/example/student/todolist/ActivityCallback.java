package com.example.student.todolist;

public interface ActivityCallback {
    void onPostSelected(int pos);
}
