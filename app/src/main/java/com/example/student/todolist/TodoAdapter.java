package com.example.student.todolist;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;

public class TodoAdapter extends RecyclerView.Adapter<TodoHolder>{
    private ArrayList<ToDoItem> toDoItems = new ArrayList();
    public ActivityCallback activityCallBack;

    public TodoAdapter(ArrayList<ToDoItem> toDoItems, ActivityCallback activityCallback){
        this.toDoItems = new ArrayList(toDoItems);
        this.activityCallBack = activityCallback;
    }

    @Override
    public TodoHolder onCreateViewHolder(ViewGroup parent, int i) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        return new TodoHolder(view);
    }

    @Override
   public void onBindViewHolder(TodoHolder holder,int pos) {
        holder.titleText.setText(toDoItems.get(pos).title);

    }
        @Override
                public int getItemCount(){
            return 1;
        }

}
